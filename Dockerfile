FROM node:19-alpine

RUN npm install --global pnpm@8.1.0 @go-task/cli && \
	pnpm config set store-dir .pnpm-store